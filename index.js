'use strict';

var Project   = require('nexrender').Project;
var renderer  = require('nexrender').renderer;

var project = new Project({
  template: 'test2.aepx',
  composition: 'MainComp',
  settings: {
    outputModule: 'Lossless',
    outputExt: 'mov'
  },
  assets: [
    {
      type: 'project',
      src: '/Users/dmitrymishin/projects/tmp/aetest/templates/test2.aepx',
      name: 'test2.aepx',
    },
    {
      type: 'image',
      src: '/Users/dmitrymishin/projects/tmp/aetest/templates/avatar2.png',
      name: 'test',
    },
    {
      type: 'mp4',
      src: '/Users/dmitrymishin/Desktop/test3.mp4',
      name: 'test.mp4',
    },
  ]
});

var rendered = renderer.render('/Applications/Adobe After Effects CC 2019/aerender', project);

const startTime = new Date();
rendered.then(function() {
  console.log('time:', new Date() - startTime);
})
